# Lab 8

I chose to test [Google's dig app](https://toolbox.googleapps.com/apps/dig/). This is an online analog of linux DNS lookup utility `dig`.

## Test cases

### Case 1. A request functionality

Description: The test case checks the basic function of this utility: entering and sending A requests to the DNS server via the Web interface.

Precondition: The browser is open and on the target website. A request selected (default state).

Step 1: Find the request bar element on the page

Step 2: Enter existing website to the request bar

Step 3: Press enter

Step 4: Find result table and verify founded IP address

Expected result: founded IP address is correct and correspond to entered domain name

### Case 2. AAAA request functionality

Description: The test case checks the basic function of this utility: entering and sending AAAA queries to the DNS server via the Web interface. Checks if switching between query types works

Precondition: The browser is open and on the target website. A request selected (default state).

Step 1: Find the request bar element on the page

Step 2: Enter existing website to the request bar (do not press enter)

Step 3: Found button for AAAA request type

Step 4: Click founded button

Step 5: Find result table and verify founded IP address

Expected result: founded IP address is correct and correspond to entered domain name

### Case 3. PTR request functionality

Description: The test case checks the basic function of this utility: entering and sending PTR queries to the DNS server via the Web interface. Checks if switching between query types works

Precondition: The broswser is open and on the target website. A request selected (default state).

Step 1: Find the request bar element on the page

Step 2: Enter existing website to the request bar (do not press enter)

Step 3: Found button for PTR request type

Step 4: Click founded button

Step 5: Find result table and verify founded domain name

Expected result: founded domain name is correct and correspond to entered IP address