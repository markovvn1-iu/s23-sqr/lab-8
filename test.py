import argparse
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


def parse_args():
	parser = argparse.ArgumentParser(prog='Test Google dig web app')
	parser.add_argument('--gitlab-ci', action='store_true', help='Test in Gitlab CI')
	return parser.parse_args()


def test_google_dig(driver):
	driver.implicitly_wait(10) # seconds

	driver.get("https://toolbox.googleapps.com/apps/dig/")
	assert "Dig" in driver.title

	elem = driver.find_element(By.ID, "domain")
	elem.clear()
	elem.send_keys("markovvn1.me")
	elem.send_keys(Keys.RETURN)

	elem = driver.find_element(By.CSS_SELECTOR, "#response-table pre.pre_wrap ~ pre.pre_wrap")
	assert elem.is_displayed()

	assert elem.text == "94.103.85.148"
	


if __name__ == "__main__":
	args = parse_args()
	if args.gitlab_ci:
		driver = webdriver.Remote(options=webdriver.FirefoxOptions(), command_executor='http://selenium__standalone-firefox:4444/wd/hub')
	else:
		driver = webdriver.Firefox()
	try:
		test_google_dig(driver)
	finally:
		driver.close()